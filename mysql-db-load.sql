-- MySQL dump 10.13  Distrib 8.0.26, for macos10.15 (x86_64)
--
-- Host: localhost    Database: automobiles
-- ------------------------------------------------------
-- Server version	8.0.26

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `automobiles`
--

DROP TABLE IF EXISTS `automobiles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `automobiles` (
                               `id` bigint NOT NULL AUTO_INCREMENT,
                               `color` varchar(255) DEFAULT NULL,
                               `make` varchar(50) DEFAULT NULL,
                               `modle` varchar(255) DEFAULT NULL,
                               `owner` varchar(255) DEFAULT NULL,
                               `vin` varchar(255) DEFAULT NULL,
                               `year` int NOT NULL,
                               `model` varchar(255) DEFAULT NULL,
                               PRIMARY KEY (`id`),
                               UNIQUE KEY `UK_o3v8p8qw2xjb3xc2bp4y3fmkw` (`vin`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `automobiles`
--

LOCK TABLES `automobiles` WRITE;
/*!40000 ALTER TABLE `automobiles` DISABLE KEYS */;
INSERT INTO `automobiles` VALUES (1,'Silver','Nissan',NULL,'Rob Wing','ABC12345XT',2008,'Xterra'),(2,'Blue','Nissan',NULL,'Barb Wing','ABC12345RG',2016,'Rogue'),(3,'Red','Mazda',NULL,'Rob Wing','ABC12345B2',1985,'B4000'),(4,'Black','Mercury',NULL,'Rob Wing','ABC12345LX',1996,'Lynx'),(5,'Fern Green','Dodge',NULL,'Rob Wing','ABC12345DS',1998,'Stratus'),(6,'Blue','Ford',NULL,'Barb Wing','ABC12345WS',2005,'Windstar'),(7,'Red','Oldsmobile',NULL,'Rob Wing','ABC12345CUT',1988,'Cutlass'),(8,'Green','Ford',NULL,'Rob Wing','ABC12345MUS',1965,'Mustang');
/*!40000 ALTER TABLE `automobiles` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-09-28  8:06:15
